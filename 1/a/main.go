package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
)

func main() {
	var v, result int64

	// Import inputs file
	f, err := os.Open("input")
	check(err)
	defer f.Close()

	// read file line by line
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		n := scanner.Text()

		// Convert to int and add result
		if len(n) > 0 {
			v, err = strconv.ParseInt(n, 10, 64)
			check(err)
			result += v
		} else {
			break
		}
	}

	// Display frequency
	fmt.Println(result)

}

func check(e error) {
	if e != nil {
		panic(e)
	}
}
