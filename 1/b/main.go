package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
)

func main() {
	var input []int64

	// Import inputs file
	file, err := os.Open("input")
	check(err)
	defer file.Close()

	// read file line by line into a slice
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		n := scanner.Text()

		// Convert to int and add result to input slice
		if len(n) > 0 {
			v, err := strconv.ParseInt(n, 10, 64)
			check(err)
			input = append(input, v)
		} else {
			break
		}
	}

	// Find first dual visited frequency
	r := search(input)

	// Display frequency
	fmt.Println(r)
}

func search(input []int64) int64 {
	var f int64
	frequencies := make(map[int64]bool)

	// loop indefinitely over the list until we find a
	// frequency we already 'visited'
	for {
		for _, val := range input {
			frequencies[f] = true
			f += val
			if frequencies[f] {
				return f
			}
		}
	}
}

func check(e error) {
	if e != nil {
		panic(e)
	}
}
