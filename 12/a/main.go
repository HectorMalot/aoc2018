package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
)

var (
	pp     = make(plan, 35)
	rounds = 2000
)

type plan map[string]string

type pot struct {
	num   int
	l     *pot
	r     *pot
	plant string // . or #
	next  string // . or #
}

func main() {
	input := readFile("input")
	// 1. create base
	base := input[0][15:]
	var lastPot *pot
	numPots := 0
	delta := 0
	for i, c := range base {
		p := &pot{i, lastPot, nil, string(c), "."}
		if lastPot != nil {
			lastPot.r = p
		}
		lastPot = p
		numPots++
	}

	// 2. create key
	// ..### => .
	for _, row := range input[2:] {
		pp[row[0:5]] = string(row[9])
	}

	// 3. Simulate
	leftPot := lastPot.left(numPots - 1)
	rightPot := lastPot
	currentPot := leftPot
	lastSum := 0
	for i := 0; i < rounds; i++ {
		if i%10000 == 0 {
			fmt.Println("Round: ", i)
			fmt.Println("numpots: ", numPots)
		}

		// 3a. make sure at least 2x . at lhs and rhs of chain
		for n := 0; n < 2; n++ {
			if leftPot.plant == "#" || leftPot.r.plant == "#" {
				leftPot.l = &pot{leftPot.num - 1, nil, leftPot, ".", "."}
				leftPot = leftPot.l
				numPots++
			}
			if rightPot.plant == "#" || rightPot.l.plant == "#" {
				rightPot.r = &pot{rightPot.num + 1, rightPot, nil, ".", "."}
				rightPot = rightPot.r
				numPots++
			}
		}

		// 3b. make sure no more than 2x . at lhs and rhs of chain
		for n := 0; n < 2; n++ {
			if leftPot.plant == "." && leftPot.r.plant == "." && leftPot.r.r.plant == "." {
				leftPot = leftPot.r
				leftPot.l = nil
				numPots--
			}
			if rightPot.plant == "." && rightPot.l.plant == "." && rightPot.l.l.plant == "." {
				rightPot = rightPot.l
				rightPot.r = nil
				numPots--
			}
		}

		// 4. calculate

		for {
			currentPot.calculate()
			if currentPot.r == nil {
				currentPot = leftPot
				break
			}
			currentPot = currentPot.r
		}

		// 5. commit
		for {
			currentPot.commit()
			if currentPot.r == nil {
				currentPot = leftPot
				break
			}
			currentPot = currentPot.r
		}

		sum := 0
		for {
			if currentPot.plant == "#" {
				sum += currentPot.num
			}
			if currentPot.r == nil {
				currentPot = leftPot
				break
			}
			currentPot = currentPot.r
		}
		delta = sum - lastSum
		lastSum = sum
	}

	// 6. calculate result
	sum := 0
	for {
		if currentPot.plant == "#" {
			sum += currentPot.num
		}
		if currentPot.r == nil {
			currentPot = leftPot
			break
		}
		currentPot = currentPot.r
	}
	fmt.Println("Result after 2000 iterations: ", sum, " Delta: ", delta)
	fmt.Println("Result after 50000000000 iterations", sum+(50000000000-2000)*delta)
}

func (p *pot) commit() {
	p.plant = p.next
}

func (p *pot) calculate() {
	key := fmt.Sprintf("%s%s%s%s%s", p.left(2).plant, p.left(1).plant, p.plant, p.right(1).plant, p.right(2).plant)
	p.next = pp[key]
}

func (p *pot) left(n int) *pot {
	if p.l == nil {
		return &pot{0, nil, p, ".", "."}
	}
	if n == 1 {
		return p.l
	}
	return p.l.left(n - 1)
}

func (p *pot) right(n int) *pot {
	if p.r == nil {
		return &pot{0, p, nil, ".", "."}
	}
	if n == 1 {
		return p.r
	}
	return p.r.right(n - 1)
}

// Utility functions
func readFile(f string) []string {
	var r []string

	// Import inputs file
	file, err := os.Open(f)
	check(err)
	defer file.Close()

	// read file line by line into a slice
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		r = append(r, scanner.Text())
	}

	return r
}

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func toInt(s string) int {
	r, _ := strconv.ParseInt(s, 10, 64)
	return int(r)
}
