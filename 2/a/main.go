package main

import (
	"bufio"
	"fmt"
	"os"
)

func main() {
	var duals, triples int
	input := readFile("input")

	// iterate over each ID, confirming for each if it has characters that are
	// included exactly two or three times

	for _, id := range input {
		has2, has3 := checkID(id)
		if has2 {
			duals++
		}
		if has3 {
			triples++
		}
	}

	// Display checksum
	fmt.Println(duals * triples)
}

// checkID returns if the ID has exactly 2 and/or 3 repeating characters
func checkID(id string) (bool, bool) {
	// Inefficient search
	var duplicate, triplicate bool
	for _, r1 := range id {
		var count int
		for _, r2 := range id {
			if r1 == r2 {
				count++
			}
		}
		if count == 2 {
			duplicate = true
		}
		if count == 3 {
			triplicate = true
		}
	}
	return duplicate, triplicate
}

func readFile(f string) []string {
	var r []string

	// Import inputs file
	file, err := os.Open(f)
	check(err)
	defer file.Close()

	// read file line by line into a slice
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		r = append(r, scanner.Text())
	}
	return r
}

func check(e error) {
	if e != nil {
		panic(e)
	}
}
