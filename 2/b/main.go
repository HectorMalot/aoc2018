package main

import (
	"bufio"
	"fmt"
	"os"
)

func main() {
	input := readFile("input")

	// iterate over each ID, look for other IDs that are almost similar
	for i, id := range input {
		found, s := findSimilar(id, input[i+1:len(input)])
		if found {
			fmt.Println(s)
		}
	}
}

func findSimilar(id string, ids []string) (bool, string) {
	// iterate over all remaining IDs
	for _, id2 := range ids {
		var count int
		var s []rune
		// count the number of matching characters
		for i, r1 := range id {
			if r1 == rune(id2[i]) {
				count++
				s = append(s, r1)
			}
		}
		// if all but one character matches, return the match
		if count == len(id)-1 {
			return true, string(s)
		}
	}
	return false, ""
}

func readFile(f string) []string {
	var r []string

	// Import inputs file
	file, err := os.Open(f)
	check(err)
	defer file.Close()

	// read file line by line into a slice
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		r = append(r, scanner.Text())
	}
	return r
}

func check(e error) {
	if e != nil {
		panic(e)
	}
}
