package main

import (
	"bufio"
	"fmt"
	"os"
	"regexp"
	"strconv"
)

func main() {
	input := readFile("input")

	var claims []claim
	// iterate over each line, create a claim struct
	for _, line := range input {
		claims = append(claims, claimFromString(line))
	}

	// iterate over each claim, increase counter on claimed areas
	cloth := map[int64]map[int64](int64){}
	for _, claim := range claims {
		for i := claim.X; i < claim.X+claim.w; i++ {
			if cloth[i] == nil {
				cloth[i] = make(map[int64]int64)
			}
			for j := claim.Y; j < claim.Y+claim.h; j++ {
				cloth[i][j]++
			}
		}
	}

	// check for each claim if it has any double bookings
	for _, claim := range claims {
		if checkClaim(claim, cloth) {
			fmt.Println(claim.ID)
		}
	}

}

func checkClaim(claim claim, cloth map[int64]map[int64](int64)) bool {
	for i := claim.X; i < claim.X+claim.w; i++ {
		for j := claim.Y; j < claim.Y+claim.h; j++ {
			if cloth[i][j] > 1 {
				return false
			}
		}
	}
	return true
}

func readFile(f string) []string {
	var r []string

	// Import inputs file
	file, err := os.Open(f)
	check(err)
	defer file.Close()

	// read file line by line into a slice
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		r = append(r, scanner.Text())
	}
	return r
}

func check(e error) {
	if e != nil {
		panic(e)
	}
}

type claim struct {
	// #1 @ 551,185: 21x10
	ID int64
	X  int64
	Y  int64
	w  int64
	h  int64
}

func claimFromString(str string) claim {
	r, _ := regexp.Compile(`#(\d+) @ (\d+),(\d+): (\d+)x(\d+)`)
	m := r.FindStringSubmatch(str)

	return claim{toInt(m[1]), toInt(m[2]), toInt(m[3]), toInt(m[4]), toInt(m[5])}
}

func toInt(s string) int64 {
	r, _ := strconv.ParseInt(s, 10, 64)
	return r
}
