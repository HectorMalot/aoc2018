package main

import (
	"bufio"
	"fmt"
	"os"
	"regexp"
	"sort"
	"strconv"
	"time"
)

const (
	shiftStart eventType = iota
	shiftAsleep
	shiftAwake
)

func main() {
	input := readFile("input")

	// convert to slice of events
	events := inputToEvents(input)

	// sort slice by time
	sort.Slice(events, func(i, j int) bool { return events[i].Time.Before(events[j].Time) })

	// create all guards
	guards := guardsFromEvents(events)

	var maxGuard *guard
	for _, g := range guards {
		if maxGuard == nil {
			maxGuard = g
		}
		if g.sleeps[int(g.maxMinute())] > maxGuard.sleeps[int(maxGuard.maxMinute())] {
			maxGuard = g
		}
	}

	fmt.Println("selected guard: ", maxGuard.ID)
	fmt.Println("selected minute: ", maxGuard.maxMinute())
	fmt.Println("solution: ", maxGuard.ID*maxGuard.maxMinute())

	fmt.Println("stats for guard: ", maxGuard.ID)
	for i := 0; i < 60; i++ {
		fmt.Println("minute: ", i, " asleep: ", maxGuard.sleeps[i])
	}
}

func readFile(f string) []string {
	var r []string

	// Import inputs file
	file, err := os.Open(f)
	check(err)
	defer file.Close()

	// read file line by line into a slice
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		r = append(r, scanner.Text())
	}
	return r
}

func check(e error) {
	if e != nil {
		panic(e)
	}
}

type event struct {
	Time      time.Time
	EventType eventType
	GuardID   int64
	Original  string
}

type guard struct {
	ID                int64
	sleeps            map[int]int64
	totalMinutesSlept int64
}

type eventType int64

func inputToEvents(input []string) []event {
	// Example inputs:
	// [1518-10-26 00:38] wakes up
	// [1518-03-18 00:12] falls asleep
	// [1518-05-23 00:01] Guard #2027 begins shift
	const timeFormat = "2006-01-02 15:04"
	events := []event{}

	for _, str := range input {

		// Parse time
		t, _ := time.Parse(timeFormat, str[1:17])

		// Parse Event
		var e eventType
		switch string(str[19]) {
		case "w":
			e = shiftAwake
		case "f":
			e = shiftAsleep
		case "G":
			e = shiftStart
		default:
			panic("could not parse event")
		}

		// Parse guard if event allows
		var g int64
		if e == shiftStart {
			r, _ := regexp.Compile(`Guard #(\d+) begins shift`)
			m := r.FindStringSubmatch(str)
			g = toInt(m[1])
		}

		// Create event
		event := event{t, e, g, str}
		events = append(events, event)
	}
	return events
}

func guardsFromEvents(events []event) map[int64]*guard {
	// requires events to be sorted ascending in time!
	guards := map[int64]*guard{}
	var currentGuard *guard

	for i, e := range events {
		if e.EventType == shiftStart {
			// create guard if it doesnt exist
			if _, ok := guards[e.GuardID]; !ok {
				guards[e.GuardID] = &guard{e.GuardID, map[int]int64{}, 0}
			}
			// set current guard
			currentGuard = guards[e.GuardID]
		} else {
			// 2 options here:
			//   1. falls asleep until end of shift
			//   2. falls asleep until wake-up moment
			if e.EventType == shiftAsleep {
				for m := e.Time.Minute(); m < 59; m++ {
					// stop counting sleep if we encounter a wake event
					if events[i+1].EventType == shiftAwake && events[i+1].Time.Minute() == m {
						break
					}
					currentGuard.sleeps[m]++
					currentGuard.totalMinutesSlept++
				}
			}
		}
	}
	return guards
}

func (g guard) maxMinute() int64 {
	max := int64(0)
	maxMinute := int64(0)
	for k, v := range g.sleeps {
		if v > max {
			maxMinute = int64(k)
			max = v
		}
	}
	return int64(maxMinute)
}

func toInt(s string) int64 {
	r, _ := strconv.ParseInt(s, 10, 64)
	return r
}
