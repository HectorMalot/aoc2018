package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
)

func main() {
	input := readFile("input")[0]
	bytes := []byte(input)

	result := 11042 // answer from part 1
	for i := 65; i < (65 + 26); i++ {
		polymer := removeUnit(bytes, byte(i))
		r := len(optimize(polymer))

		if r < result {
			result = r
		}

		fmt.Println(string(byte(i)), r)
	}

	fmt.Println("Shortest possible polymer: ", result)
}

func readFile(f string) []string {
	var r []string

	// Import inputs file
	file, err := os.Open(f)
	check(err)
	defer file.Close()

	// read file line by line into a slice
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		r = append(r, scanner.Text())
	}
	return r
}

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func toInt(s string) int64 {
	r, _ := strconv.ParseInt(s, 10, 64)
	return r
}

func optimize(b []byte) []byte {
	r := removeDuplicates(b)
	if len(r) == len(b) {
		return r
	} else {
		return optimize(r)
	}
}

func removeDuplicates(bytes []byte) []byte {
	var r []byte
	for i, b := range bytes {
		if i == len(bytes)-1 {
			break
		}
		if bytes[i+1] == b+32 || bytes[i+1] == b-32 {
			bytes[i] = 0
			bytes[i+1] = 0
		}
	}

	for _, b := range bytes {
		if b != 0 {
			r = append(r, b)
		}
	}

	return r
}

func removeUnit(bytes []byte, b byte) []byte {
	var r []byte
	for _, b2 := range bytes {
		if b2 != b && b2 != b+32 {
			r = append(r, b2)
		}
	}
	return r
}
