package main

import (
	"bufio"
	"fmt"
	"os"
	"regexp"
	"strconv"
)

func main() {
	input := readFile("input")
	places := inputToCoordinates(input)

	// Approach:
	// generate a 2d slice of points with nearest point
	// determine who is on an edge, exlude thse from answer set
	// sum remaining and determine largest

	// Generate the slice
	maxX, maxY := findMax(places)
	maxX = maxX + 10
	maxY = maxY + 10

	area := make([][]int, maxX)
	for x := 0; x < maxX; x++ {
		area[x] = make([]int, maxY)
		for y := 0; y < maxY; y++ {
			area[x][y] = nearestPlace(places, x, y).ID
		}
	}

	// Determine edges to exlude
	var ids []int
	for x := 0; x < maxX; x++ {
		ids = append(ids, area[x][0])
		ids = append(ids, area[x][maxY-1])
	}
	for y := 0; y < maxY; y++ {
		ids = append(ids, area[0][y])
		ids = append(ids, area[maxX-1][y])
	}
	ids = unique(ids)
	// fmt.Println(ids)

	// create a slice of remaining edges to consider
	possibleLocations := []place{}
	for _, p := range places {
		if !contains(p, ids) {
			possibleLocations = append(possibleLocations, p)
		}
	}
	// fmt.Println(possibleLocations)

	// sum the area for each remaining location
	r := make(map[int]int)
	for x := 0; x < maxX; x++ {
		for y := 0; y < maxY; y++ {
			r[area[x][y]]++
		}
	}

	// display list of locations with area sizes
	for _, loc := range possibleLocations {
		fmt.Println("Location ID: ", loc.ID, " has area: ", r[loc.ID])
	}

	// return the actual answer
	var maxSize int
	for _, loc := range possibleLocations {
		if r[loc.ID] > maxSize {
			maxSize = r[loc.ID]
		}
	}
	fmt.Println(maxSize)
}

func contains(p place, ids []int) bool {
	for _, id := range ids {
		if p.ID == id {
			return true
		}
	}
	return false
}

func readFile(f string) []string {
	var r []string

	// Import inputs file
	file, err := os.Open(f)
	check(err)
	defer file.Close()

	// read file line by line into a slice
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		r = append(r, scanner.Text())
	}
	return r
}

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func toInt(s string) int {
	r, _ := strconv.ParseInt(s, 10, 64)
	return int(r)
}

func inputToCoordinates(strings []string) []place {
	var coordinates []place

	r, _ := regexp.Compile(`(\d+), (\d+)`)
	for i, str := range strings {
		m := r.FindStringSubmatch(str)
		coordinates = append(coordinates, place{i, toInt(m[1]), toInt(m[2])})
	}
	return coordinates
}

func findMax(cs []place) (int, int) {
	// find max values
	var maxX, maxY int
	for _, c := range cs {
		if c.X > maxX {
			maxX = c.X
		}
		if c.Y > maxY {
			maxY = c.Y
		}
	}

	return maxX, maxY
}

type place struct {
	ID, X, Y int
}

func nearestPlace(sp []place, x, y int) place {
	r := sp[0]
	contested := false
	for _, p := range sp {
		if p.distance(x, y) == r.distance(x, y) {
			contested = true
		}
		if p.distance(x, y) < r.distance(x, y) {
			r = p
			contested = false
		}
	}
	if contested == false {
		return r
	}
	return place{}
}

func (p place) distance(x, y int) int {
	return absolute(x-p.X) + absolute(y-p.Y)
}

func absolute(n int) int {
	if n < 0 {
		return n * -1
	}
	return n
}

func unique(intSlice []int) []int {
	keys := make(map[int]bool)
	list := []int{}
	for _, entry := range intSlice {
		if _, value := keys[entry]; !value {
			keys[entry] = true
			list = append(list, entry)
		}
	}
	return list
}
