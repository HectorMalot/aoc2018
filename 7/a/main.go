package main

import (
	"bufio"
	"fmt"
	"os"
	"regexp"
	"sort"
	"strconv"
)

func main() {
	input := readFile("input")
	nodes := inputToNodes(input)

	// find nodes that can be executed (i.e. have no parents)
	// sort if there are multiple nodes
	// execute the first one
	// add the ID to the answer list
	// remove the node from the parents list of all other nodes
	// redo loop until done
	var answer string
	for {
		node, done := findNextNode(nodes)
		if done {
			break
		}
		answer = fmt.Sprintf("%s%s", answer, node.ID)
		node.exec()
		fmt.Println("Executing node: ", node.ID)
		nodes = removeNode(nodes, node)
	}
	fmt.Println(answer)
}

func readFile(f string) []string {
	var r []string

	// Import inputs file
	file, err := os.Open(f)
	check(err)
	defer file.Close()

	// read file line by line into a slice
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		r = append(r, scanner.Text())
	}
	return r
}

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func toInt(s string) int {
	r, _ := strconv.ParseInt(s, 10, 64)
	return int(r)
}

type node struct {
	ID       string
	parents  []*node
	children []*node
}

func inputToNodes(strings []string) []*node {
	nodes := make(map[string]*node, 100)

	r, _ := regexp.Compile(`Step ([A-Z]) must be finished before step ([A-Z]) can begin.`)
	for _, str := range strings {
		m := r.FindStringSubmatch(str)
		// add first node if it doesnt exist
		if nodes[m[1]] == nil {
			nodes[m[1]] = &node{m[1], []*node{}, []*node{}}
		}
		// add second node if it doesnt exist
		if nodes[m[2]] == nil {
			nodes[m[2]] = &node{m[2], []*node{}, []*node{}}
		}

		// add dependencies
		nodes[m[1]].children = append(nodes[m[1]].children, nodes[m[2]])
		nodes[m[2]].parents = append(nodes[m[2]].parents, nodes[m[1]])
	}

	var sn []*node
	for _, node := range nodes {
		sn = append(sn, node)
	}
	return sn
}

func (n *node) exec() {
	// remove dependency on self from all children
	for _, node := range n.children {
		node.parents = removeNode(node.parents, n)
	}
}

func findNextNode(sn []*node) (*node, bool) {
	// find possible nodes
	var possibleNodes []*node
	for _, n := range sn {
		if len(n.parents) == 0 {
			possibleNodes = append(possibleNodes, n)
		}
	}

	if len(possibleNodes) == 0 {
		return nil, true
	}

	// return if there is only one
	if len(possibleNodes) == 1 {
		return possibleNodes[0], false
	}

	// Otherwise find highest in abc order (nodes are A-Z)
	sort.Slice(possibleNodes, func(i, j int) bool { return possibleNodes[i].ID < possibleNodes[j].ID })

	return possibleNodes[0], false
}

func removeNode(sn []*node, n *node) []*node {
	for i, node := range sn {
		if node.ID == n.ID {
			return append(sn[:i], sn[i+1:]...)
		}
	}
	fmt.Println("Did not find node to remove")
	return sn
}
