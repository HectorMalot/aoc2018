package main

import (
	"bufio"
	"fmt"
	"os"
	"regexp"
	"sort"
	"strconv"
)

const (
	numWorkers = 5
	jobDelay   = 60
)

// ACHOQRXSEKUGMYIWDZLNBFTJVP

func main() {
	input := readFile("input")
	nodes := inputToNodes(input)
	answer := ""
	answerLength := len(nodes)

	// Create the specified number of workers
	var workers []*worker
	for i := 0; i < numWorkers; i++ {
		workers = append(workers, &worker{i, 0, nil})
	}

	tick := 0
	for {

		// Find the available jobs (sorted ascending)
		possibleNodes := findAvailableNodes(nodes)

		// Find available workers
		availabeWorkers := findAvailableWorkers(workers)

		// assign work to available workers
		for _, n := range possibleNodes {
			for _, w := range availabeWorkers {
				if w.Available() {
					fmt.Println("Worker ", w.id, " starting on node: ", n.ID, " in round: ", tick)
					w.Start(n)
					break
				}
			}
		}

		for _, w := range workers {
			if n, done := w.Tick(); done {
				answer = fmt.Sprintf("%s%s", answer, n.ID)
				fmt.Println("Worker ", w.id, " finished node   : ", n.ID, " in round: ", tick)
				nodes = removeNode(nodes, n)
			}
		}

		// Increase our counter
		tick++

		// break if we're done
		if len(answer) == answerLength {
			break
		}
	}

	fmt.Println(tick)
	// AHQXCORUSEKGYZMWIDLNBTFJVP
}

type worker struct {
	id    int
	timer int
	node  *node
}

func (w *worker) Available() bool {
	return w.node == nil
}

func (w *worker) Start(n *node) {
	w.node = n
	n.worker = w
	w.timer = n.duration
}

func (w *worker) Tick() (*node, bool) {
	if w.node != nil {
		w.timer--
		if w.timer == 0 {
			n := w.node
			n.exec()
			w.node = nil
			return n, true
		}
	}
	return nil, false
}

func readFile(f string) []string {
	var r []string

	// Import inputs file
	file, err := os.Open(f)
	check(err)
	defer file.Close()

	// read file line by line into a slice
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		r = append(r, scanner.Text())
	}
	return r
}

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func toInt(s string) int {
	r, _ := strconv.ParseInt(s, 10, 64)
	return int(r)
}

type node struct {
	ID       string
	parents  []*node
	children []*node
	duration int
	worker   *worker
}

func inputToNodes(strings []string) []*node {
	nodes := make(map[string]*node, 100)

	r, _ := regexp.Compile(`Step ([A-Z]) must be finished before step ([A-Z]) can begin.`)
	for _, str := range strings {
		m := r.FindStringSubmatch(str)
		// add first node if it doesnt exist
		if nodes[m[1]] == nil {
			nodes[m[1]] = &node{m[1], []*node{}, []*node{}, int([]byte(m[1])[0]) - 4, nil} // ascii -64 +60
		}
		// add second node if it doesnt exist
		if nodes[m[2]] == nil {
			nodes[m[2]] = &node{m[2], []*node{}, []*node{}, int([]byte(m[2])[0]) - 4, nil} // ascii -64 +60
		}

		// add dependencies
		nodes[m[1]].children = append(nodes[m[1]].children, nodes[m[2]])
		nodes[m[2]].parents = append(nodes[m[2]].parents, nodes[m[1]])
	}

	var sn []*node
	for _, node := range nodes {
		sn = append(sn, node)
	}
	return sn
}

func (n *node) exec() {
	// remove dependency on self from all children
	for _, node := range n.children {
		node.parents = removeNode(node.parents, n)
	}
}

func findNextNode(sn []*node) (*node, bool) {
	// find possible nodes
	possibleNodes := findAvailableNodes(sn)

	if len(possibleNodes) == 0 {
		return nil, true
	}

	// return if there is only one
	if len(possibleNodes) == 1 {
		return possibleNodes[0], false
	}

	return possibleNodes[0], false
}

func removeNode(sn []*node, n *node) []*node {
	for i, node := range sn {
		if node.ID == n.ID {
			return append(sn[:i], sn[i+1:]...)
		}
	}
	fmt.Println("Did not find node to remove")
	return sn
}

func findAvailableNodes(sn []*node) []*node {
	var possibleNodes []*node
	for _, n := range sn {
		if len(n.parents) == 0 && n.worker == nil {
			possibleNodes = append(possibleNodes, n)
		}
	}

	// Sort the slice ascending
	sort.Slice(possibleNodes, func(i, j int) bool { return possibleNodes[i].ID < possibleNodes[j].ID })

	return possibleNodes
}

func findAvailableWorkers(sw []*worker) []*worker {
	var possibleWorkers []*worker
	for _, w := range sw {
		if w.node == nil {
			possibleWorkers = append(possibleWorkers, w)
		}
	}
	return possibleWorkers
}
