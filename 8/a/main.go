package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

func main() {
	input := readFile("input")
	node := newNode(input)
	fmt.Println(node.metasum())

}

type node struct {
	parent      *node
	children    []*node
	metadata    []int
	length      int
	numChildren int
	numMetadata int
}

func readFile(f string) []int {
	var r1 []string
	var r []int

	// Import inputs file
	file, err := os.Open(f)
	check(err)
	defer file.Close()

	// read file line by line into a slice
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		r1 = append(r1, scanner.Text())
	}

	r2 := strings.Split(r1[0], " ")
	for _, str := range r2 {
		r = append(r, toInt(str))
	}
	return r
}

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func toInt(s string) int {
	r, _ := strconv.ParseInt(s, 10, 64)
	return int(r)
}

func newNode(seq []int) *node {
	n := &node{}
	n.numChildren = seq[0]
	n.numMetadata = seq[1]

	// Identify each of the children
	offset := 2
	for i := 0; i < n.numChildren; i++ {
		child := newNode(seq[offset:])
		offset += child.length
		n.children = append(n.children, child)
		child.parent = n
	}

	// set our total length, including children
	n.length = offset + n.numMetadata

	// set our metadata
	n.metadata = seq[offset:n.length]

	return n
}

func (n *node) metasum() int {
	sum := 0
	for _, node := range n.children {
		sum += node.metasum()
	}
	for _, num := range n.metadata {
		sum += num
	}
	return sum
}
