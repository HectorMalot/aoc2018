package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

func main() {
	input2 := readFile("input")
	node2 := newNode(input2)
	fmt.Println("Total sum of metadata: ", node2.totalmetasum())
	fmt.Println("Index sum: ", node2.indexsum())
}

type node struct {
	parent      *node
	children    []*node
	metadata    []int
	length      int
	numChildren int
	numMetadata int
}

func readFile(f string) []int {
	var r1 []string
	var r []int

	// Import inputs file
	file, err := os.Open(f)
	check(err)
	defer file.Close()

	// read file line by line into a slice
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		r1 = append(r1, scanner.Text())
	}

	r2 := strings.Split(r1[0], " ")
	for _, str := range r2 {
		r = append(r, toInt(str))
	}
	return r
}

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func toInt(s string) int {
	r, _ := strconv.ParseInt(s, 10, 64)
	return int(r)
}

func newNode(seq []int) *node {
	n := &node{}
	n.numChildren = seq[0]
	n.numMetadata = seq[1]

	// Identify each of the children
	offset := 2
	for i := 0; i < n.numChildren; i++ {
		child := newNode(seq[offset:])
		offset += child.length
		n.children = append(n.children, child)
		child.parent = n
	}

	// set our total length, including children
	n.length = offset + n.numMetadata

	// set our metadata
	n.metadata = seq[offset:n.length]

	return n
}

func (n *node) totalmetasum() int {
	sum := 0
	for _, node := range n.children {
		sum += node.totalmetasum()
	}
	sum += n.metasum()
	return sum
}

func (n *node) metasum() int {
	sum := 0
	for _, num := range n.metadata {
		sum += num
	}
	return sum
}

func (n *node) indexsum() int {
	if len(n.children) == 0 {
		return n.metasum()
	}

	// Else, get the indexsum of each child referenced by the metadata.
	// 0 if the reference does not exist
	sum := 0
	for _, m := range n.metadata {
		if m <= len(n.children) {
			sum += n.children[m-1].indexsum()
		} else {
			sum += 0
		}
	}
	return sum
}
