package main

import "fmt"

const (
// numPlayers = 452
// lastMarble = 70784
)

func main() {
	// setup
	numPlayers := 452
	lastMarble := 7078400

	// Create our players
	var players []*player
	for i := 0; i < numPlayers; i++ {
		players = append(players, &player{i + 1, make([]int, 10)})
	}

	// Create a new game with the default marble at 0
	g := &game{[]int{0}, 0, 1, players[0], players, 22, 1}

	t := uint64(0)
	for i := 0; i < lastMarble; i++ {
		g.placeMarble()
		t++
		if t == uint64(100000) {
			fmt.Println("Now at marble: ", i+1)
			t = uint64(0)
		}
	}

	max := 0
	for _, p := range players {
		// fmt.Println("player: ", p.id, " score: ", p.score())
		if p.score() > max {
			max = p.score()
		}
	}
	fmt.Println("High Score: ", max)

}

type game struct {
	marbles       []int
	index         int
	nextMarble    int
	currentPlayer *player
	players       []*player
	until23       int
	length        int
}

type player struct {
	id      int
	marbles []int
}

func (g *game) placeMarble() {
	// if it is NOT a multiple of 23, just place marble 2 places to the right

	if g.until23 != 0 {

		// we need to wrap around if we move past the end of the cirlce
		// g.index = (g.index+1)%len(g.marbles) + 1
		if g.index+2 == g.length {
			g.index = 0
		} else {
			if g.index+2 == g.length+1 {
				g.index = 1
			} else {
				if g.index+2 == g.length+2 {
					g.index = 2
				}
				g.index = g.index + 2
			}
		}

		g.marbles = append(g.marbles, 0)
		copy(g.marbles[g.index+1:], g.marbles[g.index:])
		g.marbles[g.index] = g.nextMarble
		g.length++

	} else {
		// add current marble to players score
		g.currentPlayer.marbles = append(g.currentPlayer.marbles, g.nextMarble)

		// go 7 marbles CCW
		g.index = g.index - 7
		if g.index < 0 {
			g.index = g.length + g.index
		}

		// give this marble to the player
		g.currentPlayer.marbles = append(g.currentPlayer.marbles, g.marbles[g.index])
		g.marbles = append(g.marbles[:g.index], g.marbles[g.index+1:]...)

		g.length--

		// next marbe CW is current marble
		// implicit

		g.until23 = 23
	}
	g.nextMarble++
	g.until23--

	// FOR DEBUG:
	// fmt.Println("player: ", g.currentPlayer.id, g.marbles)

	// move to the next player
	nextPlayerIndex := g.currentPlayer.id
	if nextPlayerIndex == len(g.players) {
		g.currentPlayer = g.players[0]
	} else {
		g.currentPlayer = g.players[nextPlayerIndex]
	}
}

func (p *player) score() int {
	sum := 0
	for _, m := range p.marbles {
		sum += m
	}
	return sum
}
