package main

import "fmt"

const (
	players    = 452
	lastMarble = 7078400
)

func main() {
	game := &game{}
	game.nextValue = 1

	for i := 1; i <= players; i++ {
		game.players = append(game.players, &player{i, []*marble{}})
	}
	game.currentPlayer = game.players[0]

	startingMarble := &marble{0, nil, nil}
	startingMarble.left = startingMarble
	startingMarble.right = startingMarble
	game.currentMarble = startingMarble

	for i := 0; i < lastMarble; i++ {
		game.playRound()
	}

	highscore := 0
	for _, p := range game.players {
		if p.score() > highscore {
			highscore = p.score()
		}
	}

	fmt.Println("highscore: ", highscore)
}

type marble struct {
	id    int
	left  *marble
	right *marble
}

type game struct {
	currentMarble *marble
	currentPlayer *player
	players       []*player
	nextValue     int
}

type player struct {
	id      int
	marbles []*marble
}

func (g *game) playRound() {
	// base: place a marble after the next marble
	// if mod 23, take the marble + 7 marbles to the left
	if !(g.nextValue%23 == 0) {
		m := &marble{g.nextValue, g.currentMarble.right, g.currentMarble.right.right}
		g.currentMarble.right.right.left = m
		g.currentMarble.right.right = m
		g.currentMarble = m
		g.nextValue++
	} else {
		g.currentPlayer.marbles = append(g.currentPlayer.marbles, &marble{g.nextValue, nil, nil})
		g.currentPlayer.marbles = append(g.currentPlayer.marbles, g.currentMarble.Left(7))
		g.currentMarble.Left(8).right = g.currentMarble.Left(6)
		g.currentMarble.Left(6).left = g.currentMarble.Left(8)
		g.currentMarble = g.currentMarble.Left(6)
		g.nextValue++
	}
	g.nextPlayer()
}

func (g *game) nextPlayer() {
	if g.currentPlayer.id == len(g.players) {
		g.currentPlayer = g.players[0]
	} else {
		g.currentPlayer = g.players[g.currentPlayer.id]
	}
}

func (m *marble) Left(n int) *marble {
	if n == 1 {
		return m.left
	}
	return m.left.Left(n - 1)
}

func (m *marble) Right(n int) *marble {
	if n == 1 {
		return m.right
	}
	return m.right.Right(n - 1)
}

func (p *player) score() int {
	sum := 0
	for _, m := range p.marbles {
		sum += m.id
	}
	return sum
}
